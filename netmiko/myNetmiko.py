#!/usr/bin/python3
#Program show, saves and uploads configurations on two routers simultaneously
#By Nicolai Lyngs 2019

import netmiko
#This is the ConnectHandler for vSRX_1. The ConnectHandler will create
#an SSH connection to the vSRX_1, using the credentials specified.
myConnection = netmiko.ConnectHandler(ip='192.168.9.1',
                                        device_type='juniper',
                                        username='root',
                                        password='Mayfly1',
                                        verbose=True,
                                        encoding='UTF-8')
#The ConnectHandler for the vSRX_2.
mySecondConnection = netmiko.ConnectHandler(ip='192.168.3.1',
                                        device_type='juniper',
                                        username='root',
                                        password='Mayfly1',
                                        verbose=True,
                                        encoding='UTF-8')
def menu(): #Menu for the user, to choose which function they want to use.
    print('''\n1. Show the configuration for vSRX_1 and vSRX_2\n
2. Save configuration for vSRX_1 and vSRX_2 to local machine\n
3. Upload .set changes from local machine to vSRX_1 and vSRX_2\n
4. Exit program''')
    MenuSelect()

def MenuSelect(): #Takes the user's input and return the desired function.
    inp = int(input('\n==>'))

    if inp == 1:
        return ShowConfig()
    elif inp == 2:
        return SaveConfig()
    elif inp == 3:
        return UploadToVsrxs()
    elif inp == 4:
        return ExitProgram()

def ShowConfig():#This is the function for showing the candidate configuration on the vSRX's.
    print('Retriving vSRX_1 configurations...')
    myConnection.config_mode()#enter configuration mode.
    print(myConnection.send_command('show'))#This print the vSRX_1 configurations.
    print('configurations for vSRX_1 successfully retrieved')
    myConnection.exit_config_mode()#This exit the configuration mode.

    print('Retriving vSRX_2 configurations...')
    mySecondConnection.config_mode()
    print(mySecondConnection.send_command('show'))
    print('configurations for vSRX_2 successfully retrieved')
    mySecondConnection.exit_config_mode()


def SaveConfig():#This function will save the configurations from the vSRX's.
    print('Writing vSRX_1 configs to vSrx_1ConfigSetCommands.set...')
    myConnection.config_mode()
    configSetCommands = myConnection.send_command('show | display set')#this will
    #send a command to vSRX_1 to filter the output to display set
    with open("vSrx_1ConfigSetCommands.set", "wb") as f:
        f.write(configSetCommands.encode("UTF-8")) #This will save the output to
    # a file called vSrx_1ConfigSetCommands.set
    myConnection.exit_config_mode()
    print('File successfully saved as vSrx_1ConfigSetCommands.set\n')

    print('Writing vSRX_2 configs to vSrx_2ConfigSetCommands.set...')
    mySecondConnection.config_mode()
    configSetCommands = mySecondConnection.send_command('show | display set')
    with open("vSrx_2ConfigSetCommands.set", "wb") as f:
        f.write(configSetCommands.encode("UTF-8"))
    mySecondConnection.exit_config_mode()
    print('File successfully saved as vSrx_2ConfigSetCommands.set')



def UploadToVsrxs():#This function will take the changes made to the config.set
    # document, and upload the, to the vSRX's
    print('Uploading changes to vSRX_1...')
    myConnection.config_mode()
    print(myConnection.send_config_from_file('vSrx_1ConfigSetCommands.set',exit_config_mode=False))
    #This will send the config file from the locale mashine to the vSRX_1.
    print(myConnection.commit(and_quit=True)) #This will commit the change and and quit
    # if true. If commit can't be made, then an error will be returned.
    myConnection.exit_config_mode()

    print('Uploading changes to vSRX_2...')
    mySecondConnection.config_mode()
    print(mySecondConnection.send_config_from_file('vSrx_2ConfigSetCommands.set',exit_config_mode=False))
    print(mySecondConnection.commit(and_quit=True))
    mySecondConnection.exit_config_mode()

def ExitProgram(): #This will exit the program.
    print('Exitting program...')
    myConnection.disconnect() #This disconnect the SSH connection to the vSRX's
    mySecondConnection.disconnect()
    exit()

def main():
    while True:
        menu()
        print('\n')

main()
